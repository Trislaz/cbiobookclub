---
title: "CBIO book-club"
---

Webpage for the book club 2020 @ CBIO-MinesParistech.
We plan to meet once a week, just before the CBIO meeting. 
[You can find the planning here.](https://docs.google.com/spreadsheets/d/1OMZvHeXm-XKVzCyFMbBaJsum4gii2THRr_2r8vaz6vs/edit?usp=sharing)
Trying to hold it despite the Virus ! The next sessions will take place on Zoooom

## [Pattern recognition and machine learning - C. Bishop](http://users.isr.ist.utl.pt/~wurmd/Livros/school/Bishop%20-%20Pattern%20Recognition%20And%20Machine%20Learning%20-%20Springer%20%202006.pdf)

### Session 7: Graphical Models
* Lecturer: Daniel
* Date: **26/05/20 at 3.30pm**
* Place: ZOOOOM
	* ID: 953 3162 5865
	* PWD: 4dYmRJ
* Slides : [Presentation_26_05](https://drive.google.com/file/d/1jy-aVM9TwGVOMIgor61AL7n7nuZgwQNC/view?usp=sharing)
* [An application of a Markov-Random field](https://drive.google.com/file/d/16l7q_JD5S4dBrIad2osPdCq3G9VPnD8C/view?usp=sharing)

### Session 6: Sparse kernel machines 
* Lecturer: Elise
* Date: **11/05/20 at 3.30pm**
* Place: ZOOOM.
	* ID: 990 2183 2867
	* PWD: 3zFLh3
* Slides: [Presentation_11_05](https://drive.google.com/open?id=1WDiXUBwkTdODOubpTD4995E-75V3mLKI)

### Session 5: Kernel methods
* Lecturer: Romain
* Date: **27/04/2020 at 3.15pm** 
* Place: ==ZOOM==
	* ID: 986 4532 6797
	* PWD: 083361

### Session 4: Linear models for classification, part II
* Lecturer: Arthur 
* Date : **24/02/2020**, 3.15 pm - 4.15 pm
* Place : **V005 A** Mines - 60 boulevard Saint-Michel
* Material : None

### Session 3: Linear models for classification, part I
* Lecturer: Elise 
* Date : **24/02/2020**, 3.15 pm - 4.15 pm
* Place : **L106** Mines - 60 boulevard Saint-Michel
* Material : [Presentation 24_02](https://drive.google.com/open?id=1xEgR8TA3goY-uaySrRyBE-JNAGqKds14)

### Session 2: Linear models for regression
* Lecturer: Vivien
* Date : **17/02/2020**, 3.15 pm - 4.15 pm
* Place : **L104** Mines - 60 boulevard Saint-Michel
* Material : [Presentation 17_02](https://drive.google.com/open?id=1oUBWQMZA93dD-p3Kx21db26fMLl6ZBNT)

### Session 1 : Introduction
* Lecturer: Tristan
* Date **10/02/2020**, 3.15 pm - 4.15 pm
* Place :**L106** Mines - 60 boulevard Saint-Michel
* Material : [Presentation_10_02](https://drive.google.com/open?id=190lU4ZlF9HPdygiST8iCK8K99MIQT_0o)


